package com.js.heavencoffeeapi.model.unlucky;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class UnluckyItem {
    private Long id;
    private String peopleName;
    private LocalDate dateShit;
    private Double priceHoly;
}
