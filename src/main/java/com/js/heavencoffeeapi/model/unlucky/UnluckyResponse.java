package com.js.heavencoffeeapi.model.unlucky;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class UnluckyResponse {
    private Long id;
    private Long peopleId;
    private String peopleName;
    private LocalDate dateShit;
    private Double priceHoly;
}

