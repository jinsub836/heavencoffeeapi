package com.js.heavencoffeeapi.model.people;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeopleResponse {

    private Long id;
    private String name;

}
