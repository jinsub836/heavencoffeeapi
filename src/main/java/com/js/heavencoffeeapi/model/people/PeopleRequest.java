package com.js.heavencoffeeapi.model.people;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PeopleRequest {
    private String name;
}
