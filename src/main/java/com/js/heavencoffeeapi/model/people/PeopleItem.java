package com.js.heavencoffeeapi.model.people;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeopleItem {
    private Long id;
    private String name;

}
