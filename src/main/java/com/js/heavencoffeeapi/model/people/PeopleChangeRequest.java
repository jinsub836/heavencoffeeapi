package com.js.heavencoffeeapi.model.people;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeopleChangeRequest {
    private String name;
}
