package com.js.heavencoffeeapi.repository;

import com.js.heavencoffeeapi.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeopleRepository extends JpaRepository<People , Long> {
}
