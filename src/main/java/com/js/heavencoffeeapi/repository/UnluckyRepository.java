package com.js.heavencoffeeapi.repository;

import com.js.heavencoffeeapi.entity.Unlucky;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnluckyRepository extends JpaRepository<Unlucky,Long> {
}
