package com.js.heavencoffeeapi.service;

import com.js.heavencoffeeapi.entity.People;
import com.js.heavencoffeeapi.model.people.PeopleChangeRequest;
import com.js.heavencoffeeapi.model.people.PeopleItem;
import com.js.heavencoffeeapi.model.people.PeopleRequest;
import com.js.heavencoffeeapi.model.people.PeopleResponse;
import com.js.heavencoffeeapi.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PeopleService {
    private final PeopleRepository peopleRepository;

    public People getData(long peopleId){return peopleRepository.findById(peopleId).orElseThrow();}

    public void setPeople (PeopleRequest request){
        People addData = new People();

        addData.setName(request.getName());

        peopleRepository.save(addData);
    }

    public List<PeopleItem> getPeoples(){
        List<People> originlist = peopleRepository.findAll();
        List<PeopleItem> result = new LinkedList<>();

        for(People people : originlist){
            PeopleItem addItem = new PeopleItem();
            addItem.setId(people.getId());
            addItem.setName(people.getName());

            result.add(addItem);
        } return result;
    }
    public PeopleResponse getPeople(long id){
        People originData = peopleRepository.findById(id).orElseThrow();
        PeopleResponse response = new PeopleResponse();

        response.setId(originData.getId());
        response.setName(originData.getName());

        return response;}

    public void putPeople(long id , PeopleChangeRequest request){
        People originData = peopleRepository.findById(id).orElseThrow();

        originData.setName(request.getName());

        peopleRepository.save(originData);
    }
}
