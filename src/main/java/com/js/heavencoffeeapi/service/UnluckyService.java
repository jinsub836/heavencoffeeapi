package com.js.heavencoffeeapi.service;

import com.js.heavencoffeeapi.entity.People;
import com.js.heavencoffeeapi.entity.Unlucky;
import com.js.heavencoffeeapi.model.unlucky.UnluckyChangeRequest;
import com.js.heavencoffeeapi.model.unlucky.UnluckyItem;
import com.js.heavencoffeeapi.model.unlucky.UnluckyRequest;
import com.js.heavencoffeeapi.model.unlucky.UnluckyResponse;
import com.js.heavencoffeeapi.repository.UnluckyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UnluckyService {
    private final UnluckyRepository unluckyRepository;

    public void setUnlucky(UnluckyRequest request, People peopleId){
        Unlucky addData = new Unlucky();
        addData.setPeopleId(peopleId);
        addData.setDateShit(request.getDateShit());
        addData.setPriceHoly(request.getPriceHoly());

        unluckyRepository.save(addData);
    }
    public List<UnluckyItem> getUnLucks(){
        List<Unlucky> originlist = unluckyRepository.findAll();
        List<UnluckyItem> result = new LinkedList<>();

        for(Unlucky unlucky : originlist ){
            UnluckyItem addItem = new UnluckyItem();

            addItem.setId(unlucky.getId());
            addItem.setPeopleName(unlucky.getPeopleId().getName());
            addItem.setDateShit(unlucky.getDateShit());
            addItem.setPriceHoly(unlucky.getPriceHoly());
            result.add(addItem);
        }return result;
    }

    public UnluckyResponse getUnLuck(long id){
        Unlucky originData = unluckyRepository.findById(id).orElseThrow();
        UnluckyResponse response = new UnluckyResponse();

        response.setId(originData.getId());
        response.setPeopleId(originData.getPeopleId().getId());
        response.setPeopleName(originData.getPeopleId().getName());
        response.setDateShit(originData.getDateShit());
        response.setPriceHoly(originData.getPriceHoly());

        return response;
    }
    public void putUnLuck(long id, UnluckyChangeRequest request){
        Unlucky originData = unluckyRepository.findById(id).orElseThrow();

        originData.setDateShit(request.getDateShit());
        originData.setPriceHoly(request.getPriceHoly());

        unluckyRepository.save(originData);
    }

    public void delLucks(long id){unluckyRepository.deleteById(id);}
}
