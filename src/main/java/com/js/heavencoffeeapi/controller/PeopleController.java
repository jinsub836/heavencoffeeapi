package com.js.heavencoffeeapi.controller;

import com.js.heavencoffeeapi.model.people.PeopleChangeRequest;
import com.js.heavencoffeeapi.model.people.PeopleItem;
import com.js.heavencoffeeapi.model.people.PeopleRequest;
import com.js.heavencoffeeapi.model.people.PeopleResponse;
import com.js.heavencoffeeapi.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/people")
public class PeopleController {
    private final PeopleService peopleService;

    @PostMapping("/new")
    public String setPeople(PeopleRequest request){
        peopleService.setPeople(request);
        return "입력 완료";
    }

    @GetMapping("/all")
    public List<PeopleItem> getPeoples(){
       return peopleService.getPeoples();
    }

    @GetMapping("/detail/{id}")
    public PeopleResponse getPeople(@PathVariable long id){
        return peopleService.getPeople(id);
    }

    @PutMapping("/change/people/{id}")
    public String putPeople(@PathVariable long id , PeopleChangeRequest request){
        peopleService.putPeople(id, request);
        return "수정 완료";
    }
}

