package com.js.heavencoffeeapi.controller;

import com.js.heavencoffeeapi.entity.People;
import com.js.heavencoffeeapi.model.unlucky.UnluckyChangeRequest;
import com.js.heavencoffeeapi.model.unlucky.UnluckyItem;
import com.js.heavencoffeeapi.model.unlucky.UnluckyRequest;
import com.js.heavencoffeeapi.model.unlucky.UnluckyResponse;
import com.js.heavencoffeeapi.service.PeopleService;
import com.js.heavencoffeeapi.service.UnluckyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/unlucky")
public class UnluckyController {
    private final PeopleService peopleService;
    private final UnluckyService unluckyService;

    @PostMapping("/new/{peopleId}")
    public String setUnlucky(@RequestBody UnluckyRequest request , @PathVariable long peopleId){
        People people = peopleService.getData(peopleId);
        unluckyService.setUnlucky(request,people);
        return "입력 완료";
    }

    @GetMapping("/all")
    public List<UnluckyItem> getUnLucks(){
        return unluckyService.getUnLucks();}

    @GetMapping("/detail/{id}")
    public UnluckyResponse getUnLuck(@PathVariable long id){
        return  unluckyService.getUnLuck(id);}

    @PutMapping("/change/{id}")
    public String putUnLuck(@PathVariable long id, @RequestBody UnluckyChangeRequest request) {
        unluckyService.putUnLuck(id, request);
        return "수정 완료";
    }

    @DeleteMapping("/delete-unlucky/{id}")
    public String delLucks(@PathVariable long id){
        unluckyService.delLucks(id);
        return "삭제 완료";
    }
}
